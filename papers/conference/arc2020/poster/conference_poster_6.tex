%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% a0poster Portrait Poster
% LaTeX Template
% Version 1.0 (22/06/13)
%
% The a0poster class was created by:
% Gerlinde Kettl and Matthias Weiser (tex@kettl.de)
% 
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[a0,portrait]{a0poster}

\usepackage{multicol} % This is so we can have multiple columns of text side-by-side
\columnsep=100pt % This is the amount of white space between the columns in the poster
%\columnseprule=3pt % This is the thickness of the black line between the columns in the poster

\usepackage[svgnames]{xcolor} % Specify colors by their 'svgnames', for a full list of all colors available see here: http://www.latextemplates.com/svgnames-colors

\usepackage{times} % Use the times font
%\usepackage{palatino} % Uncomment to use the Palatino font

\usepackage{graphicx} % Required for including images
\graphicspath{{../drawings/}} % Location of the graphics files
\usepackage{booktabs} % Top and bottom rules for table
\usepackage[font=small,labelfont=bf]{caption} % Required for specifying captions to tables and figures
\usepackage{amsfonts, amsmath, amsthm, amssymb} % For math fonts, symbols and environments
\usepackage{wrapfig} % Allows wrapping text around tables and figures

\usepackage{url}
\usepackage{cite}
\usepackage[all]{nowidow}
\widowpenalty=10000

\usepackage{enumitem,lipsum} 


\begin{document}

%----------------------------------------------------------------------------------------
%	POSTER HEADER 
%----------------------------------------------------------------------------------------

% The header is divided into two boxes:
% The first is 75% wide and houses the title, subtitle, names, university/organization and contact information
% The second is 25% wide and houses a logo for your university/organization or a photo of you
% The widths of these boxes can be easily edited to accommodate your content as you see fit

\begin{minipage}[b]{0.75\linewidth}
\veryHuge \color{NavyBlue} \textbf{Implementing CNNs using a Linear Array of Full Mesh CGRAs} \color{Black}\\ % Title
%\Huge\textit{An Ultra Low Energy Implementation}\\[2cm] % Subtitle
\huge \textbf{\\V. M\' ario, J.D. Lopes, M. V\'estias and J.T. de Sousa}\\[0.5cm] % Author(s)
\huge INESC-ID / University of Lisbon\\[0.4cm] % University/organization
\Large \texttt{jts@inesc-id.pt} \\
\end{minipage}
%
\begin{minipage}[b]{0.25\linewidth}
\includegraphics[width=20cm]{logo.jpg}\\
\end{minipage}

\vspace{1cm} % A bit of extra whitespace between the header and poster content

%----------------------------------------------------------------------------------------

\begin{multicols}{2} % This is how many columns your poster will be broken into, a portrait poster is generally split into 2 columns

%----------------------------------------------------------------------------------------
%	ABSTRACT
%----------------------------------------------------------------------------------------

\color{Navy} % Navy color for the abstract

%----------------------------------------------------------------------------------------
%	INTRODUCTION
%----------------------------------------------------------------------------------------

\color{DarkSlateGray}


\section*{Introduction}

\begin{itemize}
\item Machine Learning (ML) and Artificial Intelligence (AI) are becoming ubiquitous
  in data centers and workstations, especially Convolutional Neural Networks (CNN)
\item  Accelerating CNNs using GPUs is quite common
\item  Accelerating CNNs using FPGAs is less common and has been shown advantageous in terms of performance and power compared to GPUs
\item CNNs in embedded IoT edge devices is much less common due to the size and
  power consumption of the required hardware
\end{itemize}

%----------------------------------------------------------------------------------------
%	OBJECTIVES
%----------------------------------------------------------------------------------------

\section*{Objectives}

\begin{itemize}
\item Implement CNN algorithm in embedded edge devices using the Versat CGRA 
\item Present new architecture of the Versat CGRA
\item Compare with embedded CPU implementation
\item Compare qualitatively with GPU and FPGA implementation
\end{itemize}


%----------------------------------------------------------------------------------------
%	MATERIALS AND METHODS
%----------------------------------------------------------------------------------------

\section*{Previous Versat Architecture Features}

\begin{itemize}
\item Full mesh structure to eliminate place and route
\item Self, dynamically and partially reconfigurable
\item Myriad configurations generated on the fly by API
\item {\em Not spatially scalable} due to full mesh
\end{itemize}


\section*{The New Deep Versat Architecture}

\begin{center}
  \includegraphics[width=12in]{rv32-versat.pdf}
  \captionof{figure}{The RV32 Deep Versat system}
\end{center}
\vspace*{.5cm}

\begin{itemize}
\item Added spatial scalability by creating ring of full-mesh Versat cores
\item Added pre-silicon configurability of the number of Versat cores, types and numbers of functional units
\item Removed Assembly programmable controller
\item Added system level C/C++ programmable RISC-V CPU (open source picoRV32 processor)
\end{itemize}

\begin{center}
  \includegraphics[width=7.5in]{DeepVersat_new.pdf}
  \captionof{figure}{Deep Versat architecture}
\end{center}

\section*{The CNN Application: Handwritten Digit Recognition}

\begin{itemize}
\item CNN inference on a previously trained network using the {\em mnist} database and convolutional, pooling, fully connected and softmax
\item Versat used to accelerate convolutional (XX\% of CPU time) and fully connected (YY\% of CPU time) layer types
\end{itemize}
\vspace*{.5cm}
\begin{center}
  \includegraphics[width=9in]{CNN_scheme.png}
  \captionof{figure}{CNN architecture with a single convolutional layer}  
\end{center}

\begin{center}
  \includegraphics[width=7.5in]{Deep_CNN_cut.pdf}
  \captionof{figure}{Two convolutional layers}  
\end{center}

%----------------------------------------------------------------------------------------
%	RESULTS 
%----------------------------------------------------------------------------------------

\section*{Results}

%\iffalse
\begin{center}\vspace{1cm}
  \begin{tabular}{|l|c|c|c|c|c|c|c|c|}
    \hline
                         &   LUT     & FF       & RAM   & DSP   & Freq (MHz) & WNS (ns) & Time ($\mu s$) & Speedup\\
    \hline
    \hline
    RISC-V + Versat      &     7081  &     3460 &    62 &     8 & 100        & 0.521    &  9780          & 3.36   \\
    RISC-V + DeepVersat  &    40478  &    14631 &   196 &    20 & 100        & 0.292    &  1689          & 19.44  \\
    ARM + 4 GeMM         &    16706  &    17715 &    16 &    16 & 100        & NA       &  3961          & 8.25   \\
    ARM                  &       NA  &       NA &    NA &    NA & 667        & NA       & 32839          & 1      \\
    \hline
\end{tabular}
\captionof{table}{FPGA implementation + execution results}
\end{center}
%\fi

\begin{itemize}
\item Deep Versat runs 19x faster compared to an ARM Cortex-A9 processor
\item Deep Versat runs 2.3x faster compared to an ARM Cortex-A9 processor + 4 GeMM IP cores
\end{itemize}


%----------------------------------------------------------------------------------------
%	CONCLUSIONS
%----------------------------------------------------------------------------------------

\section*{Conclusions}

\begin{itemize}
\item Implemented CNN using Deep Versat
\item Proposed new Deep Versat architecture as ring array of full mesh self/dynamically/partially reconfigurable CGRAs
\item Deep Versat is controlled by C/C++ programmable RISC-V processor (picoRV32)
\item Silicon area now scales linearly with parallelism without impacting the clock frequency
\item A C++ software API has been developed for applications using Versat accelerators
\item Compared to GPU, a CGRA CNN can be used in small area footprint and low power consumption edge devices
\item Compared to FPGA, a self/dynamically/partially reconfigurable CGRA can be smaller and lower power
\end{itemize}


%----------------------------------------------------------------------------------------
%	FORTHCOMING RESEARCH
%----------------------------------------------------------------------------------------

 %----------------------------------------------------------------------------------------
%	REFERENCES
%----------------------------------------------------------------------------------------

%\nocite{*} % Print all references regardless of whether they were cited in the poster or not
%\bibliographystyle{unsrt} % Plain referencing style
%\bibliography{../Thesis_bib_DB.bib} % Use the example bibliography file sample.bib

%----------------------------------------------------------------------------------------
%	ACKNOWLEDGEMENTS
%----------------------------------------------------------------------------------------

\section*{Acknowledgements}
This work was supported by national funds through Funda\c c\~ao para a Ci\^encia
e a Tecnologia (FCT) under projects PTDC/EEI-HAC/30848/2017 and UIDB/50021/2020.

%----------------------------------------------------------------------------------------

\end{multicols}
\end{document}
