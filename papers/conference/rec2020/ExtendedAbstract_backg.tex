%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     File: ExtendedAbstract_backg.tex                               %
%     Tex Master: ExtendedAbstract.tex                               %
%                                                                    %
%     Author: Andre Calado Marta                                     %
%     Last modified : 27 Dez 2011                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% A Theory section should extend, not repeat, the background to the
% article already dealt with in the Introduction and lay the
% foundation for further work.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Background}
\label{sec:backg}


The \text{PNG} format is essentially the combined result of a first 
filtering step(prediction), followed by compression step, which uses the 
Deflate algorithm. The filtering action in the first stage and is, in 
fact, a pre-compression method that tries to make the raw image data 
more compressible, by transforming the data before compression so as to 
extract the maximum efficiency from the compression algorithm 
(Deflate).In the second stage, a non-patented lossless data compression 
algorithm based on a combination of \text{LZ77} and Huffman coding (the 
Deflate algorithm), will compress the processed data. The parameters and 
specifications for each step of the \text{PNG} encoder have only been 
specified for Method 0, the only method currently specified in the 
\text{PNG} specification~\cite{png02}, which describes how to encode a 
file into the \text{PNG} format 



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Filtering}

As mentioned above, before the compressing stage, the scanlines (image's 
lines) are filtered, to increase the Deflate compression ratio. This 
'filtering action', results in a sequence of bytes of the same size as 
the incoming sequence plus one byte. It does not reduce or compress the 
scanlines, but changes the representation of the data sequence, and 
precedes it with a byte indicating the filter type. It is important to 
note that \textbf{PNG filters are lossless}, that is, no information is 
lost~\cite{png02}. 

The scanlines can use different filter types. The used filters are 
specified by the filter type byte, at the start of each scanline. 
 Even though this byte is not part of the image data, it is included in 
the datastream sent to the compression step. 

The choice of which filter to apply is left to the encoder, which means 
that different encoders may use different filters for the same 
image~\cite{png02}. Currently, the PNG standard contains only the 
specification of Method 0, which defines five basic filter types. If 
ever extended, a different number will be used to define the new set. As 
has been said, there is no golden rule dictating which filter to apply 
to each scanline; the decision is left to the encoders. 

The filtering algorithms are applied byte-by-byte, independently of 
the image pixel depth or the colour types. The existence of the alpha 
channel does not change the way in which the image is filtered. There are no other divisions inside each image line, except for bytes. The 
filtering algorithm sees a byte datastream without any kind of types 
division (\text{RGBA} or pixels). 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{The Deflate Algorithm}

The use of the Deflate compression algorithm is specified in the \text{PNG} compression Method 0, and is based on a sliding
window of 32,768 bytes. The deflate compression is derived from the \text{LZ77}
algorithm, adding Huffman coding to improve compression results. The data
streams after Deflate compression, in \text{PNG} format, are stored in the "ZLIB"
format.
The ZLIB compression method code must specify method code 8 ("deflate"
compression) and a window size of not more than 32,768 bytes (constraint
specified in Method 0) for \text{LZ77}. Note that ZLIB compression method number and
\text{PNG} compression method number are not the same and the additional flags must not
specify a pre-set dictionary. A \text{PNG} decoder by definition must be able to
decompress any valid ZLIB data stream that satisfies these additional
constrains.

If the data to be compressed contains 16,384 bytes or fewer, the encoder can set
the window size by rounding it up to a power of 2, being 256 the minimum value
possible. This does not affect the compression ratio and decreases the memory
required for both encoding and decoding.

A compressed data set consists of a series of blocks, each block is compressed
using a combination of the \text{LZ77} algorithm and Huffman coding. The Huffman trees
for each block are independent of those from the previous or subsequent blocks; the
\text{LZ77} algorithm may use a reference to a duplicated string occurring in a
previous block, up to 32kB before~\cite{deutsch1996deflate}. 

The division of the data into different blocks allows to keep the compression 
efficiency, by using fresh trees for each block. And avoid the compressor buffer
from overflowing in case the input datastream been bigger than what the compressor
was designed for.

Each block consists in two parts: a pair of Huffman code trees that describe the
representation of the compressed data, and the compressed data. The
compressed data consist of a series of elements of two types: literal bytes, and
pointers to duplicated strings. A pointer is represented as a pair \textless
length, backward distance\textgreater. Each type of value ( literals, distance,
and lengths) in the compressed data is represented using a Huffman code, using
one code for literals and lengths and a separate code tree for distances. The
code trees for each block appear in compact form just before the compressed data
for that block. The Huffman trees themselves are compressed using Huffman
encoding\cite{deutsch1996deflate}.

\subsubsection{LZ77 Algorithm}
\label{subsubsection:LZ77}

This algorithm introduced the concept of a 'sliding window' which brought about
significant improvements in compression ratio over more primitive
algorithms~\cite{history00}. The base concept of this algorithm is a dictionary
based on pointers to represent repeated strings in a byte sequence. The pointer
is formed by the following three parts:
\begin{enumerate}
\item An {\it offset} that points out how far, from the start of the file, a
  given string is at;
\item A run {\it length}, that tells how many characters past the offset are
  part of the string;
\item The deviating character, that is, an indication that a new string was
  found;
\end{enumerate}

The string is equal to the string from {\it offset} to {\it offset+length} plus
the deviating character.

The dictionary used changes dynamically based on the sliding window as the file
is parsed for. The larger the sliding window, the larger the entries in the
dictionary will be.

\begin{figure}[!h]
  \centering
    \includegraphics[width=0.4\textwidth]{Figures/LZ77Search.png}
	\caption{LZ77 Algorithm - Sequence Search}
	\label{figLZ77}
\end{figure}
The \text{LZ77} algorithm  method has the following logic: it searches, in the 
sliding windows, for the same value as the one on its current position (
Figure \ref{figLZ77} blue area). This search is backward (Figure 
\ref{figLZ77} "value search direction" arrow), it starts from the values 
closer to the current position. When a value is found it checks the 
following bytes (Figure \ref{figLZ77} "match search direction" arrow), 
comparing them to the bytes following the current position (Figure 
\ref{figLZ77} blue area), registering only the match with the bigger length.

The algorithm after checking all previous bytes for a sequence, writes 
the pointer for the largest sequence found, and moves for the next byte 
after the sequence. This next byte on the example of the figure 
\ref{figLZ77} is the third byte 'B' outside, after the current byte and
outside the sequence. If the search does not finds a sequence it writes the 
current byte and moves to the following byte.

In the Deflate specification, the pointer is made up by the distance (
offset) and the length only. The encoded data is made up by literals (the 
literals refers to the representation of symbols on the \text{LZ77} output, as it will 
be explained further ahead) and a pair of  offset\&length.

The smaller division that \text{LZ77} uses for the symbols is the byte, meaning 
the  symbol value is a decimal between 0 and 255. The \text{LZ77} algorithm uses 
values between 0 to 285 to represent the pointers, and the literals.

The output of \text{LZ77} is a mix of pointers and literals. The literal 
represents literally the symbol in the output datastream, 
having the values from  0 to 255, meaning no change has been made to 
the  symbols binary form,(that is why it is called literal).

The remaining values 257 to 285 are used to represent the offset and
the length. For these representations extra bits are needed, 
to save the pointer correctly, since the length can have values between 3 to
258 and the distance can be between 1 to 32,768.

The value 256 is reserved. This value is used to mark the end of the block, 
it means the end of the \text{LZ77}'s  datastream. 


\subsubsection{Huffman Coding}
\label{subsubsection:HuffmanCoding}

Huffman coding creates an unprefixed tree of non-overlapping intervals, where
the length of each sequence is proportionally inversed to the probability of the
symbol needing to be encoded. The more likely a symbol is to be encoded, the
shorter its bit sequence will be~\cite{sharma2010compression}. The output from
Huffman's algorithm can be viewed as a variable-length code table for encoding a
source symbol (such as a character in a file). The algorithm derives this table
from the estimated probability or frequency of occurrence (weight), for each
possible value of the source symbol. Huffman's method can be efficiently
implemented, finding a code in time linear to the number of input weights if
these weights are sorted.

A deflate compression can use fixed Huffman codes or dynamic Huffman codes. These
two options dictate where the algorithm gets the code table for the symbols. The 
fixed Huffman codes uses a pre-made table for the Huffman Coding, decreasing the
processing time necessary for the execution of Huffman Coding. The Dynamic 
application will use the current datastream to construct the table, ensuring the 
best compression since the most frequent symbols (in the current datastream) will
have a shorter code.

These two options have different impacts on the compression and execution time 
of the deflate.
\subsection{Software-only PNG Encoder Profile}
\label{subsection:lodepngsoftware}

The profiling was performed on a Digilent ZedBoard Zynq-700 ARM/FPGA Soc Development Board, containing a Xilinx Zynq 7020 device. Its objective was to obtain real 
time data concerning the LodePNG~\cite{lodepng} encoding speed and verifying the sections 
where the hardware acceleration will be implemented to improve the 
encoder to the required specifications. The profile obtained during 
experiment has been done with non-orthodox methods since it was not 
possible to use the profile option in Xilinxs tool. The method used to 
count the algorithm time is as follows: using specific functions to 
count the actual \text{CPU} cycle, and register these values before 
(start\_time) and after (end\_time) calling a function, or at the start 
(start\_time) and end (end\_time) of the function. The subtraction of 
this value gives the number of cycles that the selected part took. 
Printing the obtained times values to a log file or to another output. 

This method has to be implemented carefully, when the counter section 
includes another section with counters the result will be incorrect. 
The obtained time values will be higher than reality, inducing errors on 
the profiling. 

The program profile used 3 Benchmarks. The first two benchmark (Paint and Forest) are two different images types in different resolutions. These benchmark were used to gain more knowledge about the PNG encoder. The third benchmark is composed by photographs with  the original \text{VGA} standard resolution (640x480 pixels). This benchmark is used to evaluate the speed up obtained after applying hardware acceleration.

Table~\ref{zedbordrsultscputime} present the duration of the encoder and its two phases (Filtering and Deflate) for the 3 benchmarks. This table allow to evaluate which phase spends most time.



\begin{table}[h]
\centering
\caption{CPU Time (ms)}
\label{zedbordrsultscputime}
\renewcommand{\arraystretch}{1.2} % more space between rows
\begin{tabular}{l|rrr}
\hline
  Picture   & Encoder          &  Filtering         &  Deflate          \\ \hline
Forest144 & 70.63 & 11.23 & 53.47 \\
Forest240 & 197.92 & 31.36 & 152.08 \\
Forest360 & 452.86 & 69.84 & 350.96 \\
Forest720 & 1,796.45 & 278.62 & 1,388.13 \\
Forest1080 & 4,145.07 & 616.98 & 3,241.07 \\ \hline
Paint144 & 39.26 & 7.82 & 27.35 \\
Paint240 & 90.98 & 21.47 & 59.65 \\
Paint360 & 175.60 & 47.12 & 107.47 \\
Paint760 & 653.77 & 187.86 & 372.99 \\
Paint1080 & 1,350.95 & 414.06 & 739.70 \\ \hline
VGA-01 & 772.92 & 84.56 & 643.06 \\
VGA-02 & 881.67 & 87.91 & 754.56 \\
VGA-03 & 676.88 & 85.97 & 550.74 \\
VGA-04 & 811.14 & 86.11 & 685.24 \\
VGA-05 & 708.81 & 84.03 & 586.99 \\
VGA-06 & 990.40 & 85.67 & 867.52
\end{tabular}
\end{table}

The comparison between the Forest and Paint benchmark allows to conclude that the Deflate duration is dependent of the image data. The Forest type image has a more diverse colour pattern than Paint type. Increasing colour diversity prolongs Deflate duration.

Table~\ref{zedbordrsultscputimesubdef} summarizes the Deflate main operations (LZ77 encoding, Huffman Tree creation, Deflate writing) execution time.

\begin{table}[h]
\centering
\caption{CPU Time - Deflate (ms)}
\label{zedbordrsultscputimesubdef}
\renewcommand{\arraystretch}{1.2} % more space between rows
\begin{tabular}{l|rrr}
\hline
Picture & LZ77         & \begin{tabular}[c]{@{}c@{}}Huffman\\ Trees\end{tabular} & \begin{tabular}[c]{@{}c@{}}Write\\ Data\end{tabular} \\ \hline
Forest144 & 32.40 & 1.45 & 17.46 \\
Forest240 & 93.29 & 3.92 & 48.36 \\
Forest360 & 225.87 & 6.83 & 109.36 \\
Forest720 & 918.60 & 10.79 & 419.88 \\
Forest1080 & 2,198.62 & 23.67 & 931.40 \\ \hline
Paint144 & 22.31 & 1.80 & 2.76 \\
Paint240 & 49.42 & 4.39 & 5.10 \\
Paint360 & 91.12 & 6.71 & 8.47 \\
Paint720 & 342.27 & 8.28 & 19.62 \\
Paint1080 & 687.73 & 18.28 & 29.17 \\ \hline
VGA-01 & 502.34 & 7.65 & 127.10 \\
VGA-02 & 632.76 & 6.39 & 108.63 \\
VGA-03 & 399.47 & 7.32 & 135.08 \\
VGA-04 & 552.02 & 6.95 & 119.48 \\
VGA-05 & 445.85 & 6.73 & 120.86 \\
VGA-06 & 761.24 & 6.91 & 93.88
\end{tabular}
\end{table}

In Deflate the most time consuming algorithm is the LZ77 as it can be 
observed in Table~\ref{zedbordrsultscputimesubdef}. The VGA benchmark 
corroborates the conclusion taken from the first two benchmarks. We can 
conclude that the LZ77 should be the first one to be accelerated. 




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Related Work}
 This section summarizes the characteristics of the implementations and 
works related to the PNG format algorithms. 

 
During the research phase, it was difficult to find related works and 
available IP implementations for a PNG encoder or its algorithms. The 
only hardware PNG Encoder found is a commercial product from Visegni 
that works with a rate of 1 pixel per clock cycle~\cite{visengi0}. 
However, there are 3 solutions for a Deflate Hardware implementation: 
One commercial product from CAST-Inc, which achieves a throughput of 100 
Gbps~\cite{castinc0}; and two open source implementations. The LZ77 
Algorithm has a variety of works about increasing its speed, despite 
none of these implementations being available.

Table~\ref{tab:resumeworks} indicates the implemented algorithm and if 
it includes the \text{LZ77}. The frequency, the throughput achieved in 
each approach and its implementation size is presented next. The window 
size used is indicated in the last column, when the approach involves 
the \text{LZ77}, 


\begin{table*}[!h]
\caption{Related Works Characteristics}
\label{tab:resumeworks}
\renewcommand{\arraystretch}{1.2} % more space between rows
\begin{tabular}{l|cccccc}
\hline
Approach                                                 & Algorithm & LZ77 & Frequency   & Size                                                         & Troughput                                               & \begin{tabular}[c]{@{}c@{}}window\\ size\end{tabular} \\ \hline
\begin{tabular}[c]{@{}l@{}}VISENGI\\ PNG Encoder\end{tabular}        & Deflate   & yes  & NA          & NA                                                           & \begin{tabular}[c]{@{}c@{}}1\\ Byte/clk\end{tabular}    & 32 KiB                                                \\
\begin{tabular}[c]{@{}l@{}}CAST\\ ZipAccel-C\end{tabular}            & Deflate   & yes  & NA          & \begin{tabular}[c]{@{}c@{}}20K-100K\\ Gates\end{tabular}     & 100 Gbps                                                & 32 KiB                                                \\
HDL-Deflate                                                          & Deflate   & yes  & 100 MHz     & \begin{tabular}[c]{@{}c@{}}9823\\ LUT\end{tabular}           & 31.79 MiB/s & 32 B                                                  \\
HT-Deflate-FPGA                                                      & Deflate   & yes  & NA          & NA                                                           & NA                                                      & NA                                                    \\
\begin{tabular}[c]{@{}l@{}}Novel Adaptive\\ Version\end{tabular}     & Huffman   & no   & 56 MHz      & \begin{tabular}[c]{@{}c@{}}3,250\\ LUT\end{tabular}          & NA                                                      & -                                                     \\
VLSI Design                                                          & LZ77      & yes  & 40 MHz      & \begin{tabular}[c]{@{}c@{}}18,397\\ transistors\end{tabular} & 12.7 MiB/s                                               & NA                                                    \\
\begin{tabular}[c]{@{}l@{}}FPGA-based \\ Systolic Array\end{tabular} & LZ77      & yes  & 105 MHz     & NA                                                           & 15.5 MiB/s                                                 & 1 KiB                                                 \\
\begin{tabular}[c]{@{}l@{}}Shiftable\\ CAM device\end{tabular}       & LZ77      & yes  & 12.5 MHz    & \begin{tabular}[c]{@{}c@{}}1,590\\ Gates\end{tabular}        & 12.5 B/s                                                & 16 B                                                  \\
CAM Approach                                                         & LZ77      & yes  & 101.309 MHz & \begin{tabular}[c]{@{}c@{}}7870\\ Slices\end{tabular}        & 96.62 MiB/s                                              & 64 B                                                 \\
Hash Architecture& LZ77      & yes  & 130 MHz & \begin{tabular}[c]{@{}c@{}}2171\\ Slices\end{tabular}        & 108.7 MiB/s & NA
\end{tabular}
\end{table*}

%The "Hash Architecture" approach use hashing techniques to compare only with 
%the most probable sequences. This makes the search dependent of the hash 
%depth, reducing the ability for a full parallel search inside the window.
The Hash approach has good working frequency and throughput, however the 
hash system may reduce the compression rate, failing to detect older 
sequences inside the window.

In the Table~\ref{tab:resumeworks} is possible to observe that from 
the available implementations, most use small search depth (window size) 
and have a slower throughput. The most recent CAM implementation achieve 
better results than a systolic array with the same frequency, making it 
a good example to follow and to try to improve it further. 