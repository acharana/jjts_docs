%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% a0poster Portrait Poster
% LaTeX Template
% Version 1.0 (22/06/13)
%
% The a0poster class was created by:
% Gerlinde Kettl and Matthias Weiser (tex@kettl.de)
% 
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[a0,portrait]{a0poster}

\usepackage{multicol} % This is so we can have multiple columns of text side-by-side
\columnsep=100pt % This is the amount of white space between the columns in the poster
\columnseprule=3pt % This is the thickness of the black line between the columns in the poster

\usepackage[svgnames]{xcolor} % Specify colors by their 'svgnames', for a full list of all colors available see here: http://www.latextemplates.com/svgnames-colors

\usepackage{times} % Use the times font
%\usepackage{palatino} % Uncomment to use the Palatino font

\usepackage{graphicx} % Required for including images
\graphicspath{{../drawings/}} % Location of the graphics files
\usepackage{booktabs} % Top and bottom rules for table
\usepackage[font=small,labelfont=bf]{caption} % Required for specifying captions to tables and figures
\usepackage{amsfonts, amsmath, amsthm, amssymb} % For math fonts, symbols and environments
\usepackage{wrapfig} % Allows wrapping text around tables and figures

\begin{document}

%----------------------------------------------------------------------------------------
%	POSTER HEADER 
%----------------------------------------------------------------------------------------

% The header is divided into two boxes:
% The first is 75% wide and houses the title, subtitle, names, university/organization and contact information
% The second is 25% wide and houses a logo for your university/organization or a photo of you
% The widths of these boxes can be easily edited to accommodate your content as you see fit

\begin{minipage}[b]{0.75\linewidth}
\veryHuge \color{NavyBlue} \textbf{K-Means Clustering on CGRA} \color{Black}\\ % Title
\Huge\textit{An Ultra Low Energy Implementation}\\[2cm] % Subtitle
\huge \textbf{J.D. Lopes, J.T. de Sousa, H. Neto and M. V\'estias}\\[0.5cm] % Author(s)
\huge INESC-ID / University of Lisbon\\[0.4cm] % University/organization
\Large \texttt{jts@inesc-id.pt} \\
\end{minipage}
%
\begin{minipage}[b]{0.25\linewidth}
\includegraphics[width=20cm]{logo.jpg}\\
\end{minipage}

\vspace{1cm} % A bit of extra whitespace between the header and poster content

%----------------------------------------------------------------------------------------

\begin{multicols}{2} % This is how many columns your poster will be broken into, a portrait poster is generally split into 2 columns

%----------------------------------------------------------------------------------------
%	ABSTRACT
%----------------------------------------------------------------------------------------

\color{Navy} % Navy color for the abstract

\begin{abstract}
In this paper, we present a k-means clustering algorithm for the
Versat architecture, a small and low power Coarse Grained
Reconfigurable Array (CGRA). This algorithm targets ultra low energy
devices, where using a GPU or FPGA accelerator is out of the
question. The Versat architecture has been enhanced with pointer
support, the possibility of using the address generators for general
purposes, and cumulative and conditional operations for the ALUs. The
algorithm is based on two hardware datapaths for the two basic steps
of the algorithm: the assignment and the update steps. The program is
fully parameterizable with the number of datapoints, centroids,
coordinates, and memory pointers for reading and writing the data.
\end{abstract}

%----------------------------------------------------------------------------------------
%	INTRODUCTION
%----------------------------------------------------------------------------------------

\color{SaddleBrown} % SaddleBrown color for the introduction

\section*{Introduction}
The k-means clustering algorithm is a widely used big data
algorithm. This algorithm has been accelerated using Graphics
Processor Units (GPUs)~\cite{farivar2008} and more recently using
Field Programmable Gate Arrays (FPGAs)~\cite{canilho2016}. Although
these implementations have been shown to improve performance and
energy consumption over General Purpose Processor (GPP)
implementations, they can hardly be used in ultra low energy devices,
such as Wireless Sensor Network (WSN) nodes. For such devices, a
smaller and more power efficient accelerator should be used, such as a
Coarse Grained Reconfigurable Array (CGRA). A dedicated hardware
accelerator may also be used but its lack of programmability can
become a serious liability in the long run.

To the best of our knowledge, a description of a k-means clustering
algorithm for a CGRA cannot be found in the literature, although
instances of the algorithm have been used in benchmark
sets~\cite{Essen2009}. This paper presents a fully parameterizable
implementation of the algorithm on a previously published CGRA -- the
Versat architecture~\cite{Lopes16}. Versat has a very small area
footprint and low energy consumption. Reconfiguration is quasi-static
(occurring only after complete loop nests are run) and partial (at the
configuration word level). Versat uses a small 16-instruction
controller to generate and/or modify hardware configurations at
runtime. The controller can be programmed in assembly language or
using a small C++ subset for which a compiler has been described
in~\cite{Santiago2017}.


%----------------------------------------------------------------------------------------
%	OBJECTIVES
%----------------------------------------------------------------------------------------

\color{DarkSlateGray} % DarkSlateGray color for the rest of the content

\section*{Main Objective}

A parameterizable k-means clustering targetting a small and energy efficient CGRA.

%----------------------------------------------------------------------------------------
%	MATERIALS AND METHODS
%----------------------------------------------------------------------------------------

\section*{Versat architecture}

\begin{center}
  \includegraphics[width=10in]{top.pdf}
  \captionof{figure}{Versat top-level entity.}
\end{center}

\begin{center}
  \includegraphics[width=10in]{de.pdf}
  \captionof{figure}{Versat data engine.}  
\end{center}


\section*{The k-means clustering algorithm}

\begin{enumerate}
\item Assignment step: each datapoint is assigned to the nearest
  centroid, using the Manhattan distance metric
\item Update step: the centroids are recalculated; the new positions
  correspond to the mean of all the data points in each cluster
\end{enumerate}

\begin{center}
  \includegraphics[width=10in]{kmeans.pdf}
  \captionof{figure}{Datapath for the assignment step.}  
\end{center}

\begin{center}
  \includegraphics[width=10in]{kmeans-acc.pdf}
  \captionof{figure}{Datapath for the update step.}  
\end{center}

The algorithm ends when the centers are unchanged after an iteration.

%----------------------------------------------------------------------------------------
%	RESULTS 
%----------------------------------------------------------------------------------------

\section*{Results}

%\iffalse
\begin{center}\vspace{1cm}
  \begin{tabular}{|c|c|c|c|c|c|}
    \hline
Core & N(nm) & A(mm\textsuperscript{2}) & RAM(kB) &  F(MHz) & P(mW)\\
    \hline
    \hline
Versat~\cite{Lopes16} & 130 & 4.2  &  46.34 & 170 &  99 \\
Versat~[here] & 130 & 5.2  &  46.34 & 170 &  132 \\
ARM Cortex A9~\cite{wang} & 40 & 4.6 &  65.54 & 800 &  500 \\
Versat [here, scaled] & 40 & 0.49  &  46.34 & 553 &  41 \\
    \hline
\end{tabular}
\captionof{table}{Integrated circuit implementation results}
\end{center}
%\fi

\begin{center}
  \begin{tabular}{ccc}
    \includegraphics[width=4.5in]{points.pdf} &
    \includegraphics[width=4.5in]{centers.pdf} &
    \includegraphics[width=4.5in]{dims.pdf} \\
  \end{tabular}
  \captionof{figure}{{\bf Iteration time} vs. {\bf datapoints} (34 centers, 30 dimensions), {\bf centers} (10064 datapoints, 15 dimensions) and {\bf dimensions} (10064 datapoints, 34 centers).}\end{center}


%----------------------------------------------------------------------------------------
%	CONCLUSIONS
%----------------------------------------------------------------------------------------

\color{SaddleBrown} % SaddleBrown color for the conclusions to make them stand out

\section*{Conclusions}

\begin{itemize}
\item A novel parameterizable k-means clustering algorithm targeting the Versat CGRA
  has been presented
\item A CGRA implementation consumes less silicon area and energy compared to GPU or FPGA, while being programmable unlike custom hardware accelerators 
\item New improvements to the Versat architecture: AGUs with support for pointers and sync signal generation; ALUs with conditional operations
\item The algorithm consists of two basic datapaths for the assignment and update steps of the algorithm, which are partially reconfigured many times during execution
  \item The execution time scales linearly with the number of
    datapoints, centers or dimensions.
\item 9.4x smaller, 3.8x faster and 46.3x more energy efficient
  compared to an ARM Cortex A9 processor
\end{itemize}

\color{DarkSlateGray} % Set the color back to DarkSlateGray for the rest of the content

%----------------------------------------------------------------------------------------
%	FORTHCOMING RESEARCH
%----------------------------------------------------------------------------------------

 %----------------------------------------------------------------------------------------
%	REFERENCES
%----------------------------------------------------------------------------------------

%\nocite{*} % Print all references regardless of whether they were cited in the poster or not
\bibliographystyle{unsrt} % Plain referencing style
\bibliography{../BIBfile.bib} % Use the example bibliography file sample.bib

%----------------------------------------------------------------------------------------
%	ACKNOWLEDGEMENTS
%----------------------------------------------------------------------------------------

\section*{Acknowledgements}
This work was supported by national funds through Funda\c c\~ao para a
Ci\^encia e a Tecnologia (FCT) with reference UID/CEC/50021/2013.

%----------------------------------------------------------------------------------------

\end{multicols}
\end{document}
