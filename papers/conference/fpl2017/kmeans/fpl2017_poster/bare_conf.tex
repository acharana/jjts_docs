\documentclass[conference,a4paper]{IEEEtran}
\usepackage{amsmath}
\usepackage[pdftex]{graphicx}
% declare the path(s) where your graphic files are
\graphicspath{{./drawings/}}
% and their extensions so you won't have to specify these with
% every instance of \includegraphics
%\DeclareGraphicsExtensions{.pdf,.jpeg,.pdf,.eps}

% correct bad hyphenation here
\hyphenation{}

\begin{document}
%
% paper title
% can use linebreaks \\ within to get better formatting as desired
\title{K-means clustering on CGRA}


% author names and affiliations
% use a multiple column layout for up to three different
% affiliations
\author{
\IEEEauthorblockN{Jo\~ao D. Lopes, Jos\'e T. de Sousa, Hor\'acio Neto}
\IEEEauthorblockA{INESC-ID Lisboa / T\'ecnico Lisboa\\
University of Lisbon\\
Emails: joao.d.lopes@ist.utl.pt, jts@inesc-id.pt, hcn@inesc-id.pt}
\and
\IEEEauthorblockN{M\'ario V\'estias}
\IEEEauthorblockA{INESC-ID Lisboa / ISEL\\
Instituto Polit\'ecnico de Lisboa \\
Email: mvestias@deetc.isel.pt}
}

% make the title area
\maketitle

\begin{abstract}
%\boldmath
In this paper we present a k-means clustering algorithm for the Versat
architecture, a small and low power Coarse Grained Reconfigurable
Array (CGRA). This algorithm targets ultra low energy devices where
using a GPU or FPGA accelerator is out of the question. The Versat
architecture has been enhanced with pointer support, the possibility
of using the address generators for general purposes, and cumulative
and conditional operations for the ALUs. The algorithm is based on two
hardware datapaths for the two basic steps of the algorithm: the
assignment and the update steps. The program is fully parameterizable
with the number of datapoints, centroids, coordinates, and memory
pointers for reading and writing the data. The execution time scales
linearly with the number of datapoints, centers or dimensions. The
results show that the new Versat core is 9.4x smaller than an ARM
Cortex A9 core, runs the algorithm 3.8x faster and consumes 46.3x less
energy.
\end{abstract}

\IEEEpeerreviewmaketitle

\section{Introduction}

The k-means clustering algorithm is a widely used data intensive
algorithm. This algorithm has been accelerated using Graphics
Processor Units (GPUs)~\cite{farivar2008} and more recently using
Field Programmable Gate Arrays (FPGAs)~\cite{canilho2016}. Although
these implementations have been shown to improve performance and
energy consumption over General Purpose Processor (GPP)
implementations, they can hardly be used in ultra low energy devices,
such as, for example, Wireless Sensor Network (WSN) nodes.  For such
devices, a smaller and more power efficient accelerator should be
used, such as a Coarse Grained Reconfigurable Array (CGRA). A
dedicated hardware accelerator may also be used but its lack of
programmability can become a serious liability in the long run.

To the best of our knowledge, a description of a k-means clustering
algorithm for a CGRA cannot be found in the literature, although
instances of the algorithm have been used in benchmark
sets~\cite{Essen2009}. This paper presents a fully parameterizable
implementation of the algorithm on a previously published CGRA -- the
Versat architecture~\cite{Lopes16}. Versat has a very small area
footprint and low energy consumption. Reconfiguration is quasi-static
(occurring only after complete loop nests are run) and partial (only
the configuration words that differ from the previous are
changed). Versat uses a small 16-instruction controller to generate
and/or modify hardware configurations at runtime. The controller can
be programmed in assembly language or using a small C++ subset for
which a compiler has been described in~\cite{Santiago2017}.

The remainder of the paper is organized as
follows. Section~\ref{sec:versat} briefly outlines the Versat
architecture. Section~\ref{sec:kernel} presents the proposed k-means
clustering algorithm. Section~\ref{sec:results} presents
implementation and performance results. Section~\ref{sec:conc}
concludes the paper.


\section{Versat architecture}
\label{sec:versat}

The Versat architecture is presented in detail in~\cite{Lopes16}. A
brief description is presented here for self containability
reasons. The top level entity is shown in figure~\ref{fig:top}. The
Controller block executes the program stored in the 2048x32bit Program
Memory (PM). The communication between a host system and Versat is
done through the 16x32bit Control Register File (CRF). The host uses
the CRF to pass parameters to a Versat program, initiate its execution
and check for completion. The Data Engine (DE) is where data intensive
computations take place. The Configuration Module (CM) is where the
controller writes DE configurations to. The configurations can be
selected for execution or partially modified. The Direct Memory Access
(DMA) module is used to access the external memory and transfer
data/instructions/configurations to/from the DE/PM/CM, respectively.
A typical Versat program running on the controller runs the DE
multiple times with different configurations, while moving data
between the DE and the external memory using the DMA.

\begin{figure}[!htb]
  \centering
  \includegraphics[width=3.5in]{top.pdf}
  \vspace{-.5cm}
  \caption{Versat top-level entity.}
  \label{fig:top}
\end{figure}

The Data Engine (DE) is a 32-bit architecture and comprises 15
functional units (FUs) as shown in figure~\ref{fig:de}. There are four
2048x32bit dual port embedded memories, six ALUs, four multipliers and
one barrel shifter. The outputs of all FUs are concatenated to form a
wide Data Bus. Each FU input can be configured to select any section
of the bus -- this implements a full mesh topology and avoids
contention.

\begin{figure}[!htb]
  \centering
  \includegraphics[width=3.5in]{de.pdf}
  \caption{Versat data engine.}  
  \vspace{-.5cm}
  \label{fig:de}
\end{figure}

The embedded memories have an Address Generator Unit (AGU) for each of
its two ports. The AGU can support two-level loop nests. Two new AGU
features have been added in this work: (1) the AGU can be bypassed and
the respective port can use as address the data input by the other
port; (2) the AGU output can be routed to the DE for general
purposes. Feature (1) provides Versat with the capability of working
with pointers. Feature (2) is useful for generating synchronization
sequences for the FUs.

In this work, the ALUs have been enhanced with conditional and
cumulative operations: they can select their own output as an operand
(internal feedback), freeing one of the inputs to be used for
conditional control. For example, an ALU can be configured as
conditional accumulator. It can also be configured to detect and
register the minimum value of certain elements of an input
sequence. The new AGUs can be used to generate control sequences for
the conditional ALU input.

\section{The k-means clustering algorithm}
\label{sec:kernel}

The k-means algorithm is one of the simplest yet widely used
clustering algorithms, due to its ease of implementation and fast
execution time. The algorithm separates the data into a set of
clusters, each having a centroid represented by the mean vector of all
data points in the cluster. Each data point is classified as being in
the cluster whose centroid is closest to it. The Euclidean distance is
a common metric, though other types of metrics can be applied, like
the Manhattan distance used in this work. After an initial position is
given to each centroid, the algorithm starts updating the position of
the centers in an iterative fashion. Each iteration is divided in two
main steps:
\begin{enumerate}
\item Assignment step: each datapoint is assigned to the nearest
  centroid, given the chosen distance metric
\item Update step: the centroids are recalculated; the new positions
  correspond to the mean of all the data points in each cluster
\end{enumerate}
The algorithm ends when the centers are unchanged after an iteration.

The Versat k-means clustering algorithm creates and partially
reconfigures two basic hardware datapaths that realize the {\em
  assignment} and {\em update} steps of the algorithm. The kernel is
written in the Versat assembly language and it is 691 instructions
long.

First, the two datapaths are created and stored in the configuration
memory. Meanwhile, the DMA loads the first datapoint chunk and
initial centroid values in memories MEM0 and MEM1, respectively. The
number of centroids times the number of coordinates is limited to
1024x32bits. This is a hard limitation of this algorithm, which can
only be overcome by using a larger embedded memory for the centroids
or by streaming the centroids as done with the datapoints.

\begin{figure}[!htb]
  \centering
  \includegraphics[width=3.5in]{kmeans.pdf}
  \vspace{-.5cm}
  \caption{Datapath for the assignment step.}  
  \vspace{-.5cm}
  \label{fig:assignment}
\end{figure}

The datapath in figure~\ref{fig:assignment} implements the assignment
step. Each datapoint is compared with all the centroids, coordinate
after coordinate. The absolute value of the coordinate difference is
accumulated in the ALU configured with the {\em ACC if A} function:
input A instructs the ALU to either register the first absolute
difference or accumulate the following ones. Input A is produced by
the address generator B of MEM1 -- this is a new Versat feature as
explained in section~\ref{sec:versat}. If $A < 0$ then the ALU
registers, otherwise it accumulates. After processing all coordinates,
the Manhattan Distance (MD) is ready. The ALU configured as {\em MIN
  if A} checks whether the current centroid is the closest yet to the
datapoint, by computing the minimum between the current MD and the
previous minimum. The current MD is delayed by 1 cycle and compared
with the current minimum. If it is smaller, then it will be the next
minimum, and the centroid index, generated by port B of MEM0, is
stored in the ALU configured as {\em REGISTER} at a its valid instant.

Finally, the point label (closest centroid index) is stored in MEM3
after its port A receives a signal from the controller at the precise
timing after all centroids are compared to the datapoint.

To process the next datapoint, the DE is partially reconfigured to
advance the addresses of the datapoint and its label in ports A of
MEM0 and MEM3, respectively. It is not possible to advance the
datapoint without reconfiguration, as this would require our AGUs to
support 3 levels of nested loops when they only support two. The 2
existing levels are used to go for all centroids and for all
coordinates of each centroid.

\begin{figure}[!htb]
  \centering
  \includegraphics[width=3.5in]{kmeans-acc.pdf}
  \vspace{-.5cm}
  \caption{Datapath for the update step.}  
  \label{fig:update}
\end{figure}

The datapath for performing the update step is shown in
figure~\ref{fig:update}. In this datapath, the point coordinates,
already loaded in the previous datapath, are read from port B of
MEM0. The point labels (centroid indices), computed in the previous
step, are read from port A of MEM3, whose port B is generating
coordinate offsets synchronized with the datapoint coordinates. An ALU
adds labels and coordinate offsets to produce the accumulated
coordinate address of the centroid to update, which is fed to port A
of MEM1. By configuration, port A can serve as the address for port B,
another of Versat's new features explained in
section~\ref{sec:versat}. Thus, the centroid accumulated coordinate is
read from port B, added to the current point coordinate and stored
back to port B at the same address. A similar loop is used for
updating the number of occurrences of each centroid in MEM2. The
number of occurrences for each centroid is incremented each time it is
addressed, using the same address fed to port A of MEM1. The two
memory read-add-write self loops take 4 clock cycles due to the
accumulated latency of these operations. Therefore, the inner loop
of the AGUs has size 4, and the outer loop size equals the number of
coordinates.

After all datapoint chunks are processed, the updated centroid
accumulated coordinates in MEM1 must be divided by the number of
occurrences in MEM2 to yield the new centroid coordinates. A
fixed-point serial divider has been added as a Controller peripheral,
taking 33 cycles per division. While a division is running, the
previously computed centroid coordinate is compared to the one stored
in MEM1 and overwritten if different. If none of the new centroids is
different from the previous, the algorithm terminates. If the point
labels are also needed as a final result, then the assignment step
must be run again for all datapoints, as labels are not kept. This
final loop represents a small overhead if the algorithm has many
iterations.

\section{Results}
\label{sec:results}

In this section, design and performance results are presented. Results
on core area, frequency and power are given. Versat's performance for
the presented k-means clustering algorithm is measured and compared to
a widely used embedded processor: the ARM Cortex A9 processor.

\subsection{IP core implementation results}

Versat has been designed using a UMC 130nm process and its main
features are shown in table~\ref{tab_asic_r}: technology node (N),
silicon area (A), embedded memory (RAM), frequency of operation (F)
and power (P). For the sake of comparison we also show the features of
the previous Versat version and of the ARM Cortex A9 processor,
implemented in a 40nm technology and optimized for power~\cite{wang}.

The Versat frequency and power results for the 130nm process have been
obtained using the Cadence IC design and simulation tools. The
frequency is reported by the place and route tool, which produces a
netlist with back-annotated layout information. Power is obtained by
simulating this netlist to get node activities, and then computing a
power figure. For synthesis, the Encounter RTL Compiler tool (v11.20)
has been used. For place and route, the Encounter Design
Implementation tool (v11.10) has been used. For simulation, the NCSIM
simulator (v12.10) has been used. To better compare with the ARM
processor, the last row in table~\ref{tab_asic_r} shows the Versat
features scaled to a 40nm technology. These results have been obtained
by applying scaling rules as explained in~\cite{borkar99}.

\begin{table}[h]
\caption{Integrated circuit implementation results}
\label{tab_asic_r}
\centering
\begin{tabular}{|c|c|c|c|c|c|}
\hline
Core & N(nm) & A(mm\textsuperscript{2}) & RAM(kB) &  F(MHz) & P(mW)\\
\hline
\hline
Versat~\cite{Lopes16} & 130 & 4.2  &  46.34 & 170 &  99 \\
\hline
Versat~[here] & 130 & 5.2  &  46.34 & 170 &  132 \\
\hline
ARM Cortex A9~\cite{wang} & 40 & 4.6 &  65.54 & 800 &  500 \\
\hline
Versat [here, scaled] & 40 & 0.49  &  46.34 & 553 &  41 \\
\hline
\end{tabular}
\end{table}

From the above results, we conclude that the current Versat core is
9.4x smaller than the ARM core and consumes 12.2x less power, assuming
both cores are clocked at their working frequencies. The Versat power
has been obtained by simulating the present k-means clustering
algorithm. The results show that the current Versat version is 24\%
larger than the previous version presented in~\cite{Lopes16}, and
consumes 32\% more power, discounting the fact that in~\cite{Lopes16}
power was measured for an FFT kernel. The ARM power figure is an
average value reported in~\cite{wang}.

\subsection{Performance results}

To study our solution's performance we used a high number of randomly
generated datasets, varying the number of data points, centroids and
coordinates. Since all iterations of the algorithm take the same time,
we measured one iteration time in clock cycles for both the Versat and
the ARM Cortex A9 systems. The real execution time is given by the
number of clock cycles per iteration divided by the clock frequency in
the 40nm process SoC, according to table~\ref{tab_asic_r}.

The Versat results have been obtained by simulating one iteration of
the algorithm using behavioral RTL simulation. The Xilinx Zynq 7010
platform has been used to measure the performance of the ARM Cortex A9
core. The ARM results have been obtained using a system timer in a
bare metal implementation of the k-means clustering algorithm.

In figure~\ref{fig:timeVSdpts}, we show the iteration time as a
function of the number of datapoints. These results show that for both
Versat and the ARM core, the execution time scales linearly with the
number of datapoints. These numbers show a Versat/ARM average speedup
of 3.8, taking into account the number of clock cycles and the working
frequencies of the cores.

\begin{figure}[!htb]
  \centering
  \includegraphics[width=3.5in]{points.png}
  \vspace{-.5cm}
  \caption{Iteration time vs. number of datapoints, for 34 centers and 30 dimensions.}
  \label{fig:timeVSdpts}
\end{figure}

In figure~\ref{fig:timeVScenters}, we show the iteration time as a
function of the number of centers. These results show that for both
cores the execution time scales linearly with the number of centers,
while the average speedup is approximately 3.6. This speedup is
slightly lower than in the previous experiment because all datapoints
can fit in the L2 cache (512kB) of the ARM system.

\begin{figure}[!htb]
  \centering
  \includegraphics[width=3.5in]{centers.png}
  \vspace{-.5cm}
  \caption{Iteration time vs. number of centers, for 10064 datapoints and 15 dimensions.}  
  \label{fig:timeVScenters}
\end{figure}

In figure~\ref{fig:timeVScoordinates}, we show the iteration time as a
function of the number of dimensions. From these results, we see that
the execution time increases linearly with the number of coordinates,
with a similar average speed up. This plot shows some non-linearity in
the ARM curve, which is attributed to fact that the L2 cache size is
exceeded for the number of dimensions studied.

\begin{figure}[!htb]
  \centering
  \includegraphics[width=3.5in]{dims.png}
  \vspace{-.5cm}
  \caption{Iteration time vs. number of dimensions, for 10064 datapoints and 34 centers.}  
  \label{fig:timeVScoordinates}
\end{figure}

The ratio of the energy consumed by the ARM core and the energy
consumed by Versat while executing the k-means clustering
algorithm is denoted $ER$ and given by
\begin{equation*}
  ER = SU \frac{P_A}{P_V} = 46.34,
\end{equation*}
where $SU=3.8$ is the average speedup, and $P_A$ and $P_V$ are the power
consumption of the ARM and Versat cores, respectively, as given in
table~\ref{tab_asic_r} for the 40nm process.


\section{Conclusion}
\label{sec:conc}

In this paper, a novel k-means clustering algorithm targeting the
Versat CGRA has been presented. Techniques for accelerating this
algorithm in GPUs and FPGAs are known but a description of a CGRA
algorithm could not be found in the literature. CGRAs are more
suitable for ultra low energy devices such as, for example, wireless
sensor network nodes, which need to collect and analyze large amounts
of data. Versat is designed to be a programmable alternative to
dedicated hardware accelerators, eliminating the risk of design
errors. New improvements to the Address Generation Units (AGUs) and
Arithmetic and Logic Units (ALUs) have been presented. AGUs have been
enhanced with pointer support and the possibility to route their
output to the data engine for general purposes. ALUs have been enhanced
with conditional ({\it if} statement support) and cumulative functions
(accumulate, sequence minimum and maximum, etc).

The proposed k-means clustering algorithm implements two basic
hardware datapaths which are dynamically and partially reconfigured
many times during execution. Our results show that the new Versat
version is 9.4x smaller than an ARM Cortex A9 processor, and runs the
k-means clustering algorithm 3.8x faster while consuming 46.3x less
energy. It should be clear from these numbers that GPUs and FPGAs
cannot compete in this arena, and that the presented solution is
useful in applications where cost and energy consumption are crucial.

% use section* for acknowledgement
\section*{Acknowledgment}

This work was supported by national funds through Funda\c c\~ao para a
Ci\^encia e a Tecnologia (FCT) with reference UID/CEC/50021/2013.

\bibliographystyle{unsrt}
\bibliography{BIBfile}

% that's all folks
\end{document}


