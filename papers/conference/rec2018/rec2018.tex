\documentclass{IEEEconfA4}

\usepackage[latin1]{inputenc}
\usepackage[dvips]{graphicx}
\usepackage{epsfig}
\usepackage{amsfonts, color}
%\usepackage{subfigure}
\usepackage{eurosym}
\usepackage{url}
\usepackage{tabularx}


%------------------------------------------------------------------------------
\begin{document}

\title{Warpbird: an Untethered System on Chip Using RISC-V Cores and the Rocket Chip Infrastructure}

% For authors with the same affiliation
%\author{Author1, Author2, ...\\
%  \begin{affiliation}
%    Affiliation\\
%    \email{\{author1, author2, ...\} @email.org}
%  \end{affiliation}}

% For authors with different affiliations
\author{Lu\'is Fiolhais, Jos\'e T. de Sousa\\
  \begin{affiliation}
    INESC-ID Lisboa / T\'ecnico Lisboa \\
    \email{luis.azenhas.fiolhais@tecnico.ulisboa.pt, jts@inesc-id.pt}
  \end{affiliation}}

\date{}

\maketitle

%------------------------------------------------------------------------------

\begin{abstract}
  The extraordinary infrastructure needed to operate any computer in
  general takes several years to develop and requires intensive
  capital investment. Hence, only large companies can afford to build
  systems using processor cores, leaving small but very innovative
  companies out of this game. It is possible to license processor
  cores from providers such as ARM but this is still too expensive for
  many small enterprises. This problem is slowly but effectively being
  addressed in the open source community by making available, free of
  charge, high quality hardware and software components which
  innovative companies can use to assemble complex systems. After a
  few attempts to create an open source processor and surrounding
  ecosystem, the RISC-V free instruction set architecture has emerged
  in full force and inspired a new wave of hope that the problem is
  finally going to be solved. This paper presents both a methodology
  for creating systems on chip (SoCs), using RISC-V cores, and a base
  open source SoC called Warpbird, which it is claimed here to be the first
  untethered SoC of its kind. An experimental evaluation of the open
  source components used and of the system itself is provided.
\end{abstract}

%------------------------------------------------------------------------------
\section{Introduction}

\subsection{Motivation}

As the world becomes more interconnected, it is time for things, in addition to
people, to join the cloud. The \textit{Internet of Things}, as it is currently
coined, offers a practically infinite number of possibilities for building
innovative devices. A piece of semiconductor IP, which can be quickly configured
to embody a device drawn from a brilliant engineer's mind, is now, more than
ever, an extremely valuable asset. Differentiation normally demands that these
Internet things have aggressive specifications in terms of area, performance and
power consumption.

Such piece of IP is indeed an SoC. In its almost 30 years of existence, the IP
market evolved from supplying small components to providing complete subsystems
with one or more processors and several software components. SoCs normally
contain one main IP system, responsible for top-level control, and a bunch of
specialized IP subsystems.

\subsection{Problem}

The current situation poses a severe problem to new entrants in the
semiconductor IP market. Having access to every hardware and software building
block needed for a complete system is expensive and/or takes too long.

Furthermore, the available open source solutions are either dependent on a
specific technology, have no parameterization options, or have non-robust
development environments~\cite{jts:rec14}. The available open source ``SoCs''
are often dependent on a specific non-free toolchain, simulator or FPGA that is
used to demo their apparent capacities. However, due to a nonexistent ecosystem,
these initiatives tend to fade out. Here, this problem is called the tethered
SoC problem. These SoCs can hardly have an existence of their own.

\subsection{Solution}

The solution may be similar to the one found for Operating Systems (OS's): free
open source OS's have emerged which in many aspects are better and more robust
than commercial and closed source alternatives.

With the emergence of the RISC-V instruction set architecture
(ISA)~\cite{riscv-isa-manual} and its open source toolchain, with special
emphasis on the Chisel3 Hardware Description Language
(HDL)~\cite{Bachrach:DAC2012} and the Rocket Chip
Generator~\cite{Asanovic:EECS-2016-17}, it is likely that the success of open
source OS's is extended to open hardware descriptions. In fact, this paper is the
continuation of the work reported in~\cite{jts:rec14}, where an attempt to
create an SoC called Blackbird, and a programming environment for building
reconfigurable systems using the OpenRISC open source processor has been made.

Unfortunately, the OpenRISC initiative has failed to create a mature enough
ecosystem of its own, and the effort revealed itself too onerous, as many
essential parts of the system were missing and/or were still being developed by
the small active community. In fact, in spite of the initial enthusiasm, the
same may happen to RISC-V.

However, the recent impetus of the RISC-V initiative is far stronger than
OpenRISC has ever enjoyed. Moreover, crowd development is becoming increasingly
easier with the advent of code sharing platforms such as {\tt github.com}. In
this new context, this paper presents a fresh attempt to investigate the
techniques used in developing Warpbird, a truly untethered SoC.

\subsection{Objectives}
This project has an objective to investigate whether a SoC built from
open source RISC-V cores can be silicon ready and built in a time
frame comparable to using commercial IP cores. To accomplish this
goal, we envision the following steps:
\begin{enumerate}\itemsep2pt
  \item build the Warpbird SoC using the Rocket Chip SoC generator, which
    contains RISC-V cores;
  \item use the Rocket Chip infrastructure to develop an environment where
    software and hardware components can be easily added, removed or modified;
  \item build a development environment encompassing all stages of
    building an SoC: hardware design, software design, RTL simulation
    and FPGA emulation, using up to date software and hardware debug
    techniques.
\end{enumerate}

\subsection{Outline}

This paper is organized as follows. In section~\ref{sec:socdesign}, the Warpbird
SoC instance implemented with completely open source modules is described.
Section~\ref{sec:verific} describes the verification methods used with the
proposed SoC. The programming tools are described in section~\ref{sec:tchain}.
In section~\ref{sec:dflow}, the development flow for Warpbird is
outlined. Finally, in section~\ref{sec:conc}, the early-stage results of this
research and its main conclusions are presented.

%------------------------------------------------------------------------------
\section{SoC design}
\label{sec:socdesign}

In order to illustrate the design of an SoC using open source components, this
research used Rocket Chip~\cite{Asanovic:EECS-2016-17}, which is an open-source
design generator that emits synthesizable RTL based on general-purpose processor
cores using the RISC-V ISA. Rocket Chip is composed of a set of utilities used
to design and parameterize a fully functional SoC. It comes with an emulator,
build and regression test environments, two RISC-V core types (in-order and
out-of-order), a RISC-V toolchain to develop software, caches, and
interconnects. The user describes systems and cores using
Chisel3~\cite{Bachrach:DAC2012} and writes its SoC configuration in the top
level domain. Rocket Chip will then generate all the Verilog code needed by the
design.

Other open source processors have been carefully considered, namely Leon
2~\cite{StamenkovicWSG06}, LatticeMico32~\cite{horst2012latticemico32}, and
OpenRISC~\cite{orpsoc}. Two decisive success factors have been identified: (1)
whether there is an active community maintaining the code, and (2) whether the
toolchain is comprehensive and open. Leon 2 is no longer maintained as the
company that gave origin to it (Gaisler Research) has been acquired by American
company Aeroflex in 2008, which in turn has merged with UK aerospace company
Cobham. LatticeMico32 still lacks crucial tools such as a good debug
environment, and the OpenRISC project does not have a sufficiently numerous and
active community. Soft processors that are not completely open such as
Microblaze and its few clones, such as the OpenFire processor~\cite{openfire},
are not considered in this study. After weighing few alternatives, the RISC-V's
Rocket Chip generator was clearly the most advantageous choice.

This paper presents Warpbird, the example SoC shown in Fig.~\ref{fig:soc}.  This
SoC is the default configuration provided by Rocket Chip for FPGAs but, unlike
the out of the box offer, Warpbird is totally independent from the Xilinx FPGA
Zynq processing System to operate. It uses its own a JTAG tap attached to the
RISC-V debug module, which is instantiated in the Warpbird design. The JTAG tap
external pins are in fact connected to the FPGA pins, which are in turn
connected to the GPIO pins of a USB device, the FT4322H mini
module~\cite{ft4232}. This device is then managed by the popular Open On Chip
Debugger tool~\cite{openocd,OpenOCD-src} known as OpenOCD, which accepts
connections over TCP/IP from different clients, among which is the very well
known GNU Debugger (GDB)~\cite{Stallman:1988:GMG} tool.

Warpbird features a simple memory mapped IO; in this first implementation only a
scratch memory is connected to the IO system, but this is enough to test the IO
read and write capabilities of the SoC. According to the target application,
peripherals may be added or removed, which also means the addition or removal of
their respective software drivers.

\begin{figure}[t]
  \centerline{\includegraphics[width=8cm]{drawings/soc.eps}}
  \vspace{0cm}\caption{The Warpbird SoC}
    \label{fig:soc}
\end{figure}


\subsection{Rocket Processor and Co-Processor}

The Rocket processor is a 5-stage in-order RISC-V processor. Rocket is equipped
with instruction and data caches of configurable size, associativities and
replacement policies, as well as instruction and data MMUs. It supports integer
operations and optionally floating-point operations (where its pipeline stages
can be configured). Branch prediction is configurable and it supports branch
target buffer (BTB), branch history table (BHT), and a return address stack
(RAS). Interrupts and exceptions of various types are also catered for. The
Rocket is master of the system's AXI4 Crossbar bus, where many slave peripherals
can be attached.

Rocket has an interface for co-processor communication. To make use of the
co-processor(s) the RISC-V ISA can be extended with new instructions. The
co-processor(s) can operate over virtual memory and has access to Rocket's data
cache and page walker, it can also interrupt the core. Furthermore, the
co-processor(s) can connect directly to external memory allowing it to have a
private cache system.

\subsection{Boot ROM}

After reset, the processor executes code permanently stored in a Boot ROM. This
code typically loads a program stored in a non-volatile memory into the central
memory. The Boot ROM is a AXI4 Crossbar slave.

\subsection{DRAM controller}

The RAM controller is the interface to an external central memory which is
normally a single or double data rate (DDR) DRAM. Newer generations of DDRs,
such as DDR3 or DDR4, can achieve better performance and higher density in terms
of stored bits per unit of area. The DRAM controller is an AXI4 Crossbar slave.

The Rocket Chip infrastructure comes with no DRAM controller. A simulation model
of the DDR is used and, when implemented in FPGA, the DDR controllers available
in the device are used.

\subsection{JTAG}

The interface for loading and debugging programs is the 4-pin JTAG interface.
The JTAG pins are connected to the JTAG tap module in the SoC, responsible for
implementing the JTAG protocol. The 4 JTAG pins are connected to an external
pod, which in turn connects to a host computer using, for example, USB or
Ethernet.

The host computer may run the program loader or debugger, or it can simply act
as a proxy for another computer where these client programs (loader or debugger)
may be run. A debugger program could also run locally on the Rocket processor
but, as the memory space for storing programs is a scarce resource, a remote
debugger is preferred for embedded systems.

\subsection{Debug module}

The debug module sits between the JTAG tap and the AXI4 Crossbar bus of which it
is a master. It also has a direct interface to the Rocket system in order to
monitor and control its execution. Being the second master of the AXI4 Crossbar
bus (Rocket is the first), the Debug module can read or write the central
memory, which is useful for setting up soft breakpoints, access variables and
other debug tasks.

%------------------------------------------------------------------------------
\section{SoC verification}
\label{sec:verific}

A processor-based system is a complex system where no exhaustive testing can be
guaranteed. At best, the system is exercised in as many ways as possible, trying
to replicate real situations and to stress critical design corners. No
one-size-fits-all solution exists and a combination of techniques is often
employed. The verification environment for the Warpbird SoC is illustrated in
Fig.~\ref{fig:verific} and comprises two SoC verification methods: RTL
simulation and FPGA emulation.

\begin{figure}[t]
    \centerline{\includegraphics[width=8cm]{drawings/toolinteraction.eps}}
    \vspace{0cm}\caption{Verification environment.}
    \label{fig:verific}
\end{figure}

\subsection{Test program and OpenOCD}
\label{verific:openocd}

Common to the two verification methods is the use of a C++ test bench (Test
Program) and the universal chip debug tool called
OpenOCD~\cite{openocd,OpenOCD-src}. Using a C++ test bench provides great
flexibility in generating test stimuli for the Device Under Test (DUT) and
checking its responses. OpenOCD provides a unified target access mechanism with
pre-built configurations supporting many commercial development platforms,
boards and interface media.

The Test Program communicates with OpenOCD using a networked socket to send
OpenOCD commands. A simple Test Program is, for instance, a Telnet client where
some target specific commands are manually issued, including commands for
loading the program, starting and halting execution, etc.

With OpenOCD, the same Test Program can seamlessly exercise the two types of
targets discussed: RTL simulation and FPGA emulation. A GDB client can also
connect to OpenOCD for program debugging: the user sets a remote target using a
specific OpenOCD port; henceforth, all GDB functionalities can be used to debug
remote embedded programs as if they were local (for further details see
section~\ref{tchain:debugger}).

\subsection{Verilog versus C++ RTL simulation}

Verilog RTL simulation is the most commonly used method for SoC verification due
to its accuracy, both in terms of timing and electrical behavior. Timing
accuracy is obtained via event driven simulation, where component delays dictate
the instants when the circuit is re-evaluated. Electrical behavior is modeled by
multi-valued logic: not only 0 and 1 states can be modeled but also other states
such as X (0/1 conflict), U (unknown) and Z (high-impedance). The most used
language in the industry for RTL design and verification is the Verilog
language.

However, Verilog RTL simulation can be slow if applied to large logic circuits
such as CPUs. It is an indispensable tool to verify the design though. Its level
of detail is adequate even for the asynchronous parts of the design, namely
clock domain crossings. Simulating systems where most interactions are software
driven requires a considerable amount of stimulus data and verification of the
response data. Writing such test benches in Verilog is hard and tedious. To
solve this problem, a Verilog Procedural Interface (VPI) is normally
employed. The VPI is a bridge between the test bench and an external C/C++
program, which actually does the complex stimuli generation and response
analysis, reducing the test bench to the role of a mere proxy. The VPI method is
better than pure Verilog RTL simulation but it is still difficult to set up
requiring much accessory code to be written.

The large time taken by RTL simulations can become a bottleneck in the project
schedule. As such, Embedded System Level (ESL) approaches have taken off in
recent years to allow rapid design and verification of complex SoCs. Among such
approaches, the SystemC initiative is one of the most prominent
ones~\cite{systemc}. SystemC is a set of C++ libraries, containing classes and
methods for designing and verifying digital circuits and systems.

A C++ model of the design is obtained from the design's Verilog description by
running a free and open source tool called Verilator (verilated design).
Simulation of this model can be faster than Verilog simulation but its time
resolution and signal representation accuracy is lower, supporting only 2-valued
logic ('0' and '1'). The C++ model created by Verilator can then be used as a
class in C++ or SystemC test benches. The Warpbird simulation environment
consists of the verilated design and a test bench written in C++. Integration
with SystemC is under way. When compiled, these files produce a software
emulator program, which communicates with the OpenOCD gateway using a TCP/IP
socket.

Rocket Chip offers a testing framework called Torture for random program
testing~\cite{torture-github} which uses both RTL simulation and ISA simulation
via the Spike simulator~\cite{spike-github}. Torture generates RISC-V programs
from small pieces of code and glues them together. At the end of each test,
Torture dumps Rocket's registers from RTL simulation and compares them to
simulation results of the same program in Spike~\cite{spike-github}. If Torture
finds an error in the core it will look for the smallest piece of code in the
generated program which creates the error and stores it. The user can then use
Torture's logs to debug the SoC.

\subsection{FPGA emulation}
\label{verific:fpga}

Often one would like to test the actual SoC even before it is available in
silicon. One of the best alternatives is to use a circuit emulator, which is
normally implemented with FPGA chips. Emulation provides the fastest
verification method and, for circuits intended to run at low frequencies,
emulation can even be real-time. As a reference, emulation speeds can be 1 to 2
orders of magnitude slower than real-time for complex SoCs implemented in the
latest and densest IC technology nodes.

In spite of its performance, emulation provides poor visibility into the
design's internal signals. When a bug is found, the designer first tries to
diagnose the problem using software methods and/or logic analyzers to observe
the interface signals. If observation of the interface signals is not
sufficient, internal signals can be observed in the FPGA, with certain
limitations, using an Integrated Logic Analyzer (ILA). The main limitations of
ILAs are the maximum sampling frequency, the number and complexity of triggers,
and the maximum amount of internal SRAM that can be used to store signals for
observation.

%------------------------------------------------------------------------------
\section{Toolchain}
\label{sec:tchain}

The RISC-V SoC has a fully operational toolchain for barebone
applications~\footnote{A barebone application runs directly on the machine,
  without any operating system, and must \textit{know} the underlying
  hardware.}. Compilation, debugging and profiling are based on the
corresponding GNU tools. Most of the tools available for RISC-V are already
mature, and most have been merged upstream (their status is available
in~\cite{riscv-tools-status}).

A similar toolchain for Linux is also available, built with Newlib and a proxy
kernel~\cite{pk-github}. The latter is the basis for the Linux RISC-V
port. There are also efforts to port other compilers and runtime environments,
\textit{e.g.}, OpenJDK, Go, Rust and OCaml.

\subsection{Cross-compiler}
\label{tchain:cross-compiler}

The main barebone cross-compiler used for RISC-V is GCC and clang/LLVM. Although
an LLVM/clang based version also exists, GCC is still needed to assemble and
link the binary. The official GCC distribution supports RISC-V as of version
7.1.

Many cross-compilers available for commercial processors are built around GCC
but, without violating its GNU General Public License (GPL), include closed
static libraries for many key functionalities, namely architectural simulation,
IO, target loading and debugger connectivity. One might be tempted to develop
clean room interface compatible processors, lured by the availability of GCC and
other GNU tools. This is a mistake, as the closed components may be very hard to
develop, even tough they represent a small part of the whole.

The RISC-V cross-compiler uses Newlib and is 100\% open source, allowing for
customization and tweaking. This possibility makes a huge difference for the
developer, who can reflect hardware changes on the cross-compiler, perhaps not
trivially, but for sure without any kind of reverse engineering. Hardware
changes may affect the command line options of the cross-compiler to select
particular configurations of the CPU and SoC. There are options to make the
compilation aware of the floating-point unit, multiplication and division units,
the clock frequency for a specific board, etc.

\subsection{Debugger}
\label{tchain:debugger}

The GNU debugger (GDB) is also available for the RISC-V SoC, and patches have
already been submitted upstream. GDB provides an extensive range of facilities
for execution tracing, halting on conditions, variable watching, etc. Usually
run as a standalone application to debug local programs, GDB can also be run as
a client/server application to debug a program in a remote system. The control
of the target is left to a server which is accessed by the client via the
\verb!target remote! command. The client/server communication is done over
TCP/IP and uses GDB's Remote Server Protocol (RSP).

The RSP server may run on the target system or on any machine that has some
means of talking to the target. In our design, the RSP server runs within
OpenOCD which, in turn, communicates via USB/JTAG with the target and acts as a
GDB proxy server. The GDB client need not be on the same machine; for it, this
process is transparent and everything works as if the GDB server was running on
the target. The RISC-V debug specification has not been finalized and
consequently the Debug Module does yet not support all OpenOCD features (at the
time of this writing the debug specification is at version
0.13~\cite{debug-spec}).

\subsection{Chisel3}
One of the biggest advantages of the Rocket Chip framework is the use of a novel
hardware description language, Chisel~\cite{Bachrach:DAC2012}. Chisel was born
out of a need to write hardware descriptions using powerful abstractions present
in modern software languages.

Chisel3 is built on top of Scala~\cite{scala}, a Java-like language with support
for functional programming and a static type system. From a top down view
Chisel3 is a Verilog macro engine, \textit{i.e.}, it generates a hardware
description by composing pre-stored and small Verilog code
snippets. Furthermore, Chisel3 features a standard library with components used
in most hardware designs: single and double precision floating-point units,
ROMs, RAMs, decoders, ripple-carry adders, booth multipliers, iterative
dividers, etc. Chisel3 is able to generate Verilog optimized for FPGA or ASIC.

Abstraction within Chisel is an important aspect of the language, as it allows
the hardware description to be parameterized, generic, and reusable. Another
important aspect is the usage of generators. Generating several instances of the
same component with different parameterizations and connections, or recursive
connections is impossible in VHDL or Verilog without rewriting a large portion
of code. Often a designer would have to use a Python script to recursively
generate the correct hardware description.

Chisel3 offers a testing environment which is described in Chisel to test
designs. There are two simulation targets, a Chisel specific C++ RTL simulator
or a Verilator target.

%------------------------------------------------------------------------------
\section{Development flow}
\label{sec:dflow}

The proposed SoC development flow is outlined in Fig.~\ref{fig:dflow}. It
follows four distinct stages, each with several iterations between development
and testing.  Given the usual complexity, tests are seldom comprehensive:
stepping back to prior stages and finding some previously uncaught issues is
quite common.

\begin{figure}[t]
    \centerline{\includegraphics[width=8cm]{drawings/dflow.eps}}
    \vspace{0cm}\caption{Development flow.}
    \label{fig:dflow}
\end{figure}

\subsection{Installation}
\label{dflow:install}

The Warpbird SoC specific deliverables are packaged in several containers:
hardware (Chisel3), RISC-V tools (spike, proxy kernel, OpenOCD), GNU toolchain
(GCC, GDB) and test environment (Torture, SoC emulator). But in order to have a
fully operational environment, some other tools must also be installed and/or
configured, namely an RTL simulator like Verilator and Chisel3 (Scala
environment and a couple of packages). After downloading Rocket Chip, setting
the development environment is as easy as running a build script, and following
the instructions to install Scala on your system.

The installation proved successful on Debian Linux (stable version Stretch) and
on macOS High Sierra (version 10.13.1).

\subsection{Peripherals}
\label{dflow:peripherals}

Once the development environment is successfully built and tested, the hardware
customization phase may begin: adding or removing modules from the Rocket core,
configuring peripherals for the SoC, or developing new peripherals suited for
the aimed application. This phase includes writing and/or rewriting Chisel3
descriptions, but also entails the upgrading of the test bench, both for its
hardware and software components.

The toolchain must be adapted to the new hardware, either via the available
options mentioned in section~\ref{tchain:cross-compiler}, or via specific
backend configuration files. When new peripherals are developed, the compiler
backend may also need to be changed to accommodate the new functionalities.

The added functionalities may bring counterparts to the programming environment,
namely by the creation of new software libraries. In that case, the test bench
also needs to encompass those changes: programs using the new functions should
be written and tested.

\subsection{Application}
\label{dflow:application}

The software application is developed in any favored IDE --- an IDE integration
is not provided. There are two straightforward ways of running and debugging the
application, each with its own advantages and disadvantages: Spike, RTL software
emulation and the FPGA SoC emulation.

Application testing with Spike has the advantage of running on the host
computer, having no need for a board. However, the ISA simulator does not run as
fast as the FPGA emulator and lacks some of the hardware features, namely the
cache simulation. Running the application using the a software RTL emulator can
simulate the system and all its peripherals at a cycle accurate level, but the
simulation will be quite slow. As such, running the application on the FPGA is
often necessary for speed and timing accuracy.

In any of the above scenarios, debugging of the target can be performed using
the GDB client, as described in section~\ref{verific:openocd}, which is very
convenient.

\subsection{Optimization}
\label{dflow:optimization}

The overall goal is to accelerate the application execution, which can be
accurately measured using FPGA emulation even though, as mentioned in
section~\ref{verific:fpga}, FPGA emulation is 1 to 2 orders of magnitude slower
than real-time. There is no profiler available in the RISC-V
toolchain. Currently, when compiling a program with the profiler option
(\verb!-pg!), GCC fails to link the binary against a library. Without the
profiler, one may use a timer to measure the execution time of critical
routines.


%------------------------------------------------------------------------------
\section{Conclusions and status}
\label{sec:conc}

The Rocket Chip framework provides a strong basis for designing SoCs based on
RISC-V cores, rendering the whole effort orders of magnitude smaller than
building hardware and software from scratch. This is the essence of a business
model consisting of building original systems from open source components.

One major barrier to entering this market is the open source licensing model of
the components. There is a lack of a standard license agreement for open source
hardware descriptions~\cite{greenbaum2011}, as most open source software
licenses are not generally applicable. Most open source IP cores are made
available using the GNU Lesser General Public License (LGPL), but adoption by
users still lacks expression.

In this paper the focus is the construction of an untethered Rocket Chip
instance called Warpbird, an SoC which can work independently and does not need
to use any resources from its simulation environment, host FPGA or any external
soft or hard IP block present in its packaging. The current Rocket Chip SoC
generator uses resources from the Xilinx FPGA in which it is packaged, namely
the ARM Cortex A9 processing system present in the Zynq devices, which is made
responsible for program loading, control and IO, and does not have any debug
infrastructure such as the Debug Module or the JTAG tap~\cite{fpga-github}. In
contrast, Warpbird can be freely placed in any FPGA or ASIC, includes the Debug
Module and the JTAG tap and has a detached memory mapped IO system. Program
loading, control and debug are taken care of via JTAG and IO can be exemplified
by just hanging a scratch memory from the IO bus. The building of Warpbird is an
ongoing process. The status of the project as of December 2017 is reported in
the following subsections.

\subsection{Hardware status}

The status of the Warpbird hardware components is summarized in
Table~\ref{tab:hwfeat}. As indicated in this table, the FPGA hardware as been
synthesized and implemented down to the bitstream, but it has not been brought
up for testing.

\begin{table}[h]
  \centering
    \begin{tabular}{|p{2cm}|p{5cm}|}
      \hline
      {\bf Item} & {\bf Comment} \\
      \hline \hline
      Rocket GPP & OK in RTL simulation; implemented but not yet run in FPGA. \\
      \hline
      Debug Module & OK in RTL simulation, implemented but not yet run in FPGA.\\
      \hline
      JTAG controller & OK in RTL simulation, implemented but not yet run in FPGA.\\
      \hline
      UART & Under Development. \\
      \hline
      I2C controller & Under Development. \\
      \hline
      SPI controller & Under Development. \\
      \hline
      DRAM controller & Non-existent, uses FPGA's or 3rd party. \\
      \hline
      Power Management Unit & Under Development. \\
      \hline
    \end{tabular}
  \caption{Warpbird FPGA hardware status}
  \label{tab:hwfeat}
\end{table}

Using the ZedBoard platform~\cite{zedboard}, containing a Xilinx Zynq-7020-1
device~\cite{zynq-7000:datasheet}, Warpbird, configured with no floating-point
unit, no branch prediction, no MMUs and with 16kB I/D caches, uses the resources
shown in Table~\ref{tab:usage-zed}. These results confirm that Chisel3 is able
to efficiently map a high level hardware description to all types of FPGA
resources present in the device.

\begin{table}[h]
  \centering
    \begin{tabular}{|l|c|}
      \hline
      LUT & 16037 \\
      \hline
      LUTRAM & 2256 \\
      \hline
      Flip-Flops & 6234 \\
      \hline
      BRAM & 4 \\
      \hline
      Frequency & 50MHz \\
      \hline
    \end{tabular}
    \caption{Warpbird Resource Usage in a Zynq-7020-1 device}
  \label{tab:usage-zed}
\end{table}

Table~\ref{tab:warpbirdfeat} shows the resources used by
the critical components in the Warpbird SoC. The results so far actually confirm
that the open source IP cores behave well when ported to a user simulation
environment.

\begin{table}[h]
  \centering
    \begin{tabular}{|p{2cm}|p{2.0cm}|p{2.0cm}|}
      \hline
      {\bf Item}    & {\bf LUTs} & {\bf BRAMs}\\
      \hline \hline
      RV64 core    & 3710        & 0 \\
      \hline
      ICache       & 224         & 1.5 \\
      \hline
      DCache       & 1418        & 1.5 \\
      \hline
      Debug Module   & 1737      & 0 \\
      \hline
    \end{tabular}
  \caption{Warpbird critical components resource usage}
  \label{tab:warpbirdfeat}
\end{table}

\subsection{Verification status}

The status of the verification components is summarized in
Table~\ref{tab:vrfeat}. FPGA emulation and RTL simulation have been run as
provided by Rocket Chip.

\begin{table}[h]
  \centering
    \begin{tabular}{|p{2cm}|p{5cm}|}
      \hline
      {\bf Item} & {\bf Comment} \\
      \hline \hline
      Test program & Not started \\
      \hline
      OpenOCD &  OK with RTL simulation \\
      \hline
      RTL simulation & Test suite and benchmarks pass\\
      \hline
      FPGA emulation &  Only out of the box features tested, no test harness present yet.\\
      \hline
    \end{tabular}
  \caption{Verification status}
  \label{tab:vrfeat}
\end{table}

\subsection{Software status}

The status of the software components is summarized in Table~\ref{tab:swfeat}.
In general, it can be said that these tools appear to be very mature, which aids
the goals of the project.

\begin{table}[h]
  \centering
    \begin{tabular}{|p{2cm}|p{5cm}|}
      \hline
      {\bf Item} & {\bf Comment} \\
      \hline \hline
      GCC for C & Programs compiled correctly \\
      \hline
      GCC for C++ & Simple programs compiled correctly\\
      \hline
      GDB & Tested with RTL and Spike; tests with FPGA emulation not started\\
      \hline
      Spike & Tested with simple programs \\
      \hline
      Profiler & Unavailable \\
      \hline
      Linux toolchain & Most of glibc has been ported \\
      \hline
      Linux OS & The provided kernel works as expected \\
      \hline
    \end{tabular}
  \caption{Software status}
  \label{tab:swfeat}
\end{table}


\section*{Acknowledgment}

This work was supported by national funds through Funda\c c\~ao para a
Ci\^encia e a Tecnologia (FCT) with reference UID/CEC/50021/2013.

%------------------------------------------------------------------------------
\bibliographystyle{IEEEtran}
\bibliography{BIBFile}
%\nocite{*}

\end{document}

%%  LocalWords:  synthesizable
